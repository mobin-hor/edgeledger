function initMap(){
    const loc = { lat: 42.3661145, lng: -71.057083};

    const map = new google.maps.Map(document.querySelector('.map')
    ,{
        zoom: 14,
        center:loc
    });


    const marker = new google.maps.marker({position: loc , map: map})


}

window.addEventListener('scroll', function(){

    if(window.scrollY>150){
        document.querySelector("#navbar").style.opacity = 0.9;

    }else
        this.document.querySelector('#navbar').style.opacity = 1;

})


$('#navbar a, .btn').on('click' , function(event){

    if(this.hash !== ''){
        event.preventDefault()

        const hash = this.hash;

        $('html,body').animate(
            {
                scrollTop : $(hash).offset().top - 100,
            },
            600
        )
    }
})